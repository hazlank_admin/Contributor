### 沉淀同学们的贡献疑问🤔️ 

- 提交社区 BUG：[https://bugs.opencloudos.tech](https://bugs.opencloudos.tech)  
- 小助手微信：qingmin0623  


 **🔅 如果要参与 gitee 上发布的编程类任务和文档类任务，是否只需要在任务发布的下方回复领取任务即可？** 

是的，回复「领取任务」，如果社区技术老师回复你，就算认领成功啦。

 **🔅 如果有部分任务没有后续的回复的话算领取成功吗？后续会有专门的老师来联系吗？** 
  
有一些任务只可以领取一次，一些任务可以多次认领。  
如果老师没有回复你，任务没有领取成功。  
如果老师回复你，任务就领取成功啦，同时你需要根据老师回复的内容主动联系老师，有疑问可以随时提出交流哦。  

 **🔅 怎么提交贡献？**   
1. Fork 仓库
2. 编写内容
3. 提交 Pull Request

[官方文档：Fork+Pull Request](https://gitee.com/help/articles/4128#article-header0)  
[测试类 Issue 贡献具体教程](https://gitee.com/OpenCloudOS/packages-testing/blob/master/README.md)

 **🔅 提交代码到哪个仓库呢？**   
如果 Issue 有明确提交地址的，同学们可以根据 Issue 描述的提交地址提交。    
  
如果没有特别说明提交地址的，建议：  
- 添加任务负责人的交流提交地址  
- 点击进该 Issue 所在的仓库，Fork 仓库，建个单独的目录放代码，提交 Pull Request   
![输入图片说明](picture/2.png)  
 
 **🔅 怎么查看合并记录？**   
点击进提交代码的仓库 -> Pull Request -> 已合并，参考：  
![输入图片说明](picture/165591684827805_.pic.jpg)  
  

 **🔅 怎么签署 CLA 协议？**    
社区升级了签署CLA的流程，同学们可以在这里完成在线签署：[http://cla.opencloudos.tech:8080](http://cla.opencloudos.tech:8080)  
p.s. 由于 CLA 协议刚升级完毕，如果暂时无法签署，可以先参与贡献，后续再补签署。  
  
 **🔅 官方提供的自动测试用例套件仍存在疑惑，请问是否有渠道和教程来指导如何编写与利用该测试套件？**    
后续我们会上传视频教程到B站等平台，目前可以先参考测试套示例库：[https://gitee.com/opencloudos-stream/test-suite-example](https://gitee.com/opencloudos-stream/test-suite-example)
