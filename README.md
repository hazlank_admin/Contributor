# OpenCloudOS 贡献指南🌟

欢迎加入 OpenCloudOS 开源操作系统社区，参与社区贡献 💝🙆🏻‍♀️🙆🏻

完成项目贡献的开发者可获得「贡献者证书」，完成难度较高或社区重要任务可获得社区周边奖励和被评选为年度「优秀贡献者」的机会。

## 关于社区

OpenCloudOS 是由操作系统、云平台、软硬件厂商与个人共同倡议发起的操作系统社区项目。成立之初，即决定成为完全开放中立的开源社区，并已通过开放原子开源基金会的 TOC 评议，确认接受社区项目捐赠。OpenCloudOS 社区将打造全面中立、开放、安全、稳定易用、高性能的 Linux 服务器操作系统为目标，与成员单位共同构建健康繁荣的国产操作系统生态。

社区沉淀了多家厂商在软件和开源生态的优势，继承了腾讯在操作系统和内核层面超过 10 年的技术积累，在云原生、稳定性、性能、硬件支持等方面均有坚实支撑，可以平等全面地支持所有硬件平台。

[💻官网](https://www.opencloudos.org)｜[💡下载 OpenCloudOS 官方版本镜像](https://www.opencloudos.org/iso)｜[🔩Bug 提交](https://bugs.opencloudos.tech)｜[🧪OpenCloudOS 实验室](https://cloud.tencent.com/lab/seriesDetail/10050?channel=c2&sceneCode=oc-git)

### 参与社区贡献

#### 在贡献之前

1、填写 [OpenCloudOS 开源贡献者信息收集表单](https://wj.qq.com/s2/11508084/1e3e/)

2、添加官方小助手微信（微信号：qingmin0623）加入【OpenCloudOS技术交流群】

- 建议你通读我们的[贡献指南](https://www.opencloudos.org/contribution)

- [签署在线 CLA](http://cla.opencloudos.tech:8080/)

#### 贡献步骤

1、进入仓库的 [Issue](https://gitee.com/organizations/OpenCloudOS/issues) 页面，选择你感兴趣的 Issue，认领和完成它

- 如果你愿意解决，请点击进该 Issue，并在评论区留言「领取任务」通知管理员与其他贡献者，如管理员回复你即完成 Issue 认领  
- [Fork](https://gitee.com/help/articles/4128#article-header0) Issue 所在的仓库/指引的仓库 到个人的仓库下
- 解决完对应的问题后，提交 PR 至 Issue 所在仓库的 master 分支   

2、完成贡献后，自行提交贡献记录至 [OpenCloudOS 贡献记录表](https://docs.qq.com/sheet/DSllCTHBoYmlOVmxB?tab=BB08J2)

- 小助手将和社区负责人一起审核贡献记录，按月联系你发放「开源贡献者」证书
- 如完成难度较高或社区重要任务可获得社区周边奖励

## OpenCloudOS 社区任务（持续更新） 🌟

我们对贡献任务进行了分类，以帮助你找到你感兴趣的任务：

#### 编程类

[编程类任务题汇总（难度：低、中、高）](https://gitee.com/OpenCloudOS/Contributor/issues/I69Y6S?from=project-issue) 

#### 文档类

[文档类任务题汇总（难度：低、中、高）](https://gitee.com/OpenCloudOS/Contributor/issues/I69Y6P?from=project-issue) 


 **贡献帮助** 
- [🌟Q&A 和教程](https://gitee.com/OpenCloudOS/Contributor/blob/master/Q&A.md)
- 邮件联系 skyeqiu@tencent.com
- 添加官方小助手微信（微信号：qingmin0623）加入【OpenCloudOS技术交流群】